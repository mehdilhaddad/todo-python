from django.shortcuts import render
import pyrebase

config = {

   'apiKey': "AIzaSyDlT_G_KsW1q9Z-d6L3XR871JHRFFhmyXI",
   'authDomain': "todo-python-8b5ae.firebaseapp.com",
   'databaseURL': "https://todo-python-8b5ae-default-rtdb.europe-west1.firebasedatabase.app",
   'projectId': "todo-python-8b5ae",
   'storageBucket': "todo-python-8b5ae.appspot.com",
   'messagingSenderId': "699696435898",
   'appId': "1:699696435898:web:fcdfb182f1b8ecb9b36883",
   'measurementId': "G-REMTCZSH3C"
}
firebase = pyrebase.initialize_app(config)

auth = firebase.auth()

def signIn(request):

    return render(request,"signIn.html")
    
def postsign(request):

    email = request.POST.get('email')

    password = request.POST.get("password")
    
    user = auth.sign_in_with_email_and_password(email,password)
    

    return render(request, "welcome.html", {"e":email})

def todoliste(request):

    return render(request, "todoliste.html")    



